Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database
- Table Users
create table users(
    -> id int (8) primary key auto_increment,
    -> name varchar (255)
    -> email varchar (255)
    -> password varchar (255)
    -> );
- Table Categories
create table categories(
    -> id int (8) primary key auto_increment,
    -> name varchar (255)
    -> );
- Table Items
create table items(
    -> id int (8) primary key auto_increment,
    -> name varchar (255),
    -> description varchar (255),
    -> price int (20),
    -> stock int (20),
    -> categories_id int (8),
    -> foreign key (categories_id) references categories(id)
    -> );

Soal 3 Memasukkan Data pada Table
- Table Users
insert into users(name,email,password) values ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
- Table Categories
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
- Table Items
insert into items(name,description,price,stock,categories_id) 
values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Soal 4 Mengambil Data dari Database
a. select id, name, email from users;
b. 	- select * from items where price>1000000;
	- select * from items where name like '%sang%';
c. select items.name, items.description, items.price, items.stock, items.categories_id, categories.name as category from items inner join categories on items.categories_id = categories.id;


Soal 5 Mengubah Data dari Database
update items set price = 2500000 where id=1;

